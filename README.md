#Net Glue Multipart Template Mail Message Module for Zend Framework 2.0

This module for ZF2 provides a simple service that creates and returns multipart/alternative
email messages by simply providing a view script name and a view model|array|traversable to the
createMessage() method of the service. The model vars are interpolated into the view script as you'd expect
and there's a filter that converts the resultant HTML into plain text automatically.

Although untested, you should be able to pass in the standard template resolver from config so you can easily drop your
view scripts into an already available view script path.

## REQUIREMENTS

* [Zend Framework 2](http://framework.zend.com)
* [Netglue ZF2 Library extensions](https://bitbucket.org/netglue/zf2) _(For the HtmlToText filter)_

## INSTALLATION

The module can be installed with [composer](http://getcomposer.org). Just add `require { "netglue/ng-mtmail" : "dev-master" }`
to your `composer.json` file

## USAGE

Within your app somewhere... Like in a controller or something,

```
#!php

use Zend\Mail\Transport\Sendmail;

$sl = $this->getServiceLocator();
$service = $sl->get('MailTemplate');
$model = array(
	'message' => 'How do?',
);
$message = $service->createMessage('example/example', $model);
$message->setSubject('Test Message')
	->addTo('me@example.com')
	->addFrom('me@example.com');

$transport = new Sendmail;
$transport->send($message);

```

In the default configuration, the view script path is setup to ModuleRoot/view
in there is an example template in example/example.phtml hence the `example/example` parameter
to `createMessage()`

The second parameter to `createMessage()` is the `ViewModel` which can be a ready intantiated `Zend\View\Model\ViewModel`,
a `Zend\View\Variables` object, an array, something that implements `Traversable` or `NULL`

You'll have to setup your own transport or get an existing one from the service manager or something...
