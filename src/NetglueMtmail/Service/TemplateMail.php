<?php
/**
 * Service provides accessors for creating email messages based on named templates
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @package	Netglue_MTMailModule
 * @link https://bitbucket.org/netglue/zf2-mtmail-module
 */
namespace NetglueMtmail\Service;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\FactoryInterface;

use Traversable;
use Zend\View as View;
use Zend\View\Model\ViewModel;

use Zend\View\Resolver as ViewResolver;
use Zend\View\Renderer as ViewRenderer;

use Netglue\Filter\HtmlToText;

use Zend\Mail\Message;
use Zend\Mime as Mime;

/**
 * Service provides accessors for creating email messages based on named templates
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @package	Netglue_MTMailModule
 * @link https://bitbucket.org/netglue/zf2-mtmail-module
 */
class TemplateMail implements FactoryInterface, ServiceLocatorAwareInterface {
	
	/**
	 * Service Locator
	 * @var ServiceLocatorInterface
	 */
	protected $serviceLocator;
	
	/**
	 * Template Resolver
	 * @var \Zend\View\Resolver\ResolverInterface|NULL
	 */
	protected $resolver;
	
	/**
	 * View Renderer
	 * @var \Zend\View\Renderer\RendererInterface|NULL
	 */
	protected $renderer;
	
	/**
	 * Set Service Locator
	 * @implements \Zend\ServiceManager\ServiceLocatorAwareInterface
	 * @return void
	 * @param ServiceLocatorInterface $serviceLocator
	 */
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
		$this->serviceLocator = $serviceLocator;
	}
	
	/**
	 * Return Service Locator
	 * @implements \Zend\ServiceManager\ServiceLocatorAwareInterface
	 * @return ServiceLocatorInterface|NULL
	 */
	public function getServiceLocator() {
		return $this->serviceLocator;
	}
	
	/**
	 * Return instance of self
	 * @implements \Zend\ServiceManager\FactoryInterface
	 * @param ServiceLocatorInterface $serviceLocator
	 * @return ContactResponder $this
	 */
	public function createService(ServiceLocatorInterface $serviceLocator) {
		return $this;
	}
	
	/**
	 * Create a new email message
	 * @param string $template
	 * @param null|array|Traversable|ViewModel $model
	 */
	public function createMessage($template, $model = NULL) {
		if(NULL === $model) {
			$model = new ViewModel;
		}
		if(!$model instanceof ViewModel && (is_array($model) || $model instanceof Traversable)) {
			$model = new ViewModel($model);
		}
		if(!$model instanceof ViewModel) {
			throw new Exception\InvalidArgumentException('The model argument must be an array, a ViewModel or an object that implements Traversable');
		}
		$model->setTemplate($template);
		$html = $this->getRenderer()->render($model);
		$filter = new HtmlToText;
		$text = $filter->filter($html);
		
		$text = new Mime\Part($text);
		$text->type = 'text/plain';
		
		$html = new Mime\Part($html);
		$html->type = 'text/html';
		
		$body = new Mime\Message;
		$body->setParts(array($text, $html));
		
		$email = new Message;
		$email->setBody($body);
		$email->getHeaders()->get('content-type')->setType('multipart/alternative');
		return $email;
	}
	
	/**
	 * Return an instance of the renderer
	 * @return Zend\View\Renderer\RendererInterface
	 */
	public function getRenderer() {
		if($this->renderer instanceof ViewRenderer\RendererInterface) {
			return $this->renderer;
		}
		$renderer = new ViewRenderer\PhpRenderer;
		$renderer->setResolver($this->getTemplateResolver());
		
		/**
		 * Retreive the view manager from the service locator
		 */
		$sl = $this->getServiceLocator();
		$viewManager = $sl->get('ViewManager');
		/**
		 * Clone the plugin manager for now.
		 * It doesn't seem to make any odds, but, when you set the plugin manager
		 * in the renderer, the renderer makes a call to $plugins->setRenderer($this)
		 * which would mean that web pages would be using a different renderer.
		 * Seeing as both are likely php renderers, this matters little, but just in case.
		 * All the view helpers will have gone through $helperPluginManager->injectRenderer($helper) on
		 * instantiation, so each viewhelper will still hold an instance of the default renderer anyway.
		 */
		$viewHelpers = clone $viewManager->getHelperManager();
		$renderer->setHelperPluginManager($viewHelpers);
		
		$this->setRenderer($renderer);
		return $renderer;
	}
	
	/**
	 * Set the view renderer
	 * @param Zend\View\Renderer\RendererInterface
	 */
	public function setRenderer(ViewRenderer\RendererInterface $renderer) {
		$this->renderer = $renderer;
	}
	
	/**
	 * Return an instance of the template resolver
	 * @return \Zend\View\Resolver\ResolverInterface
	 */
	public function getTemplateResolver() {
		if($this->resolver instanceof ViewResolver\ResolverInterface) {
			return $this->resolver;
		}
		$resolver = new ViewResolver\AggregateResolver;
		$sl = $this->getServiceLocator();
		$config = $sl->get('Config');
		$map = array();
		$stack = array();
		if(isset($config[__NAMESPACE__])) {
			$config = $config[__NAMESPACE__];
			if(is_array($config) && isset($config['template_map'])) {
				$map = $config['template_map'];
			}
			if(is_array($config) && isset($config['template_path_stack'])) {
				$stack = $config['template_path_stack'];
			}
		}
		$mapper = new ViewResolver\TemplateMapResolver($map);
		$resolver->attach($mapper);
		$stacker = new ViewResolver\TemplatePathStack;
		$stacker->addPaths($stack);
		$resolver->attach($stacker);
		$this->setTemplateResolver($resolver);
		return $resolver;
	}
	
	/**
	 * Set the template resolver
	 * @param \Zend\View\Resolver\ResolverInterface $resolver
	 * @return void
	 */
	public function setTemplateResolver(ViewResolver\ResolverInterface $resolver) {
		$this->resolver = $resolver;
	}
	
	
	
}