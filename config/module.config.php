<?php
/**
 * Base Configuration for the Netglue Multipart Template Mail Module
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @package	Netglue_MTMailModule
 * @link https://bitbucket.org/netglue/zf2-mtmail-module
 */
return array(
	
	'NetglueMtmail\Service' => array(
		'template_map' => array(),
		'template_path_stack' => array(
			__DIR__ . '/../view',
		),
	),
	
	'service_manager' => array(
		'factories' => array(
			'MailTemplate' => 'NetglueMtmail\Service\TemplateMail',
		),
	),
);
